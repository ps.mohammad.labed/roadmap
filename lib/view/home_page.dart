import 'dart:io';


import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:get/get.dart';

import 'package:image_picker/image_picker.dart';

import 'package:roadmap/controller/getx_controller.dart';
import 'package:roadmap/model/db_helper.dart';
import 'package:roadmap/view/Widget/maindrawer.dart';
import 'package:roadmap/view/Widget/searchdelegate.dart';
import 'package:roadmap/view/Widget/segmentcontroll.dart';
import 'package:roadmap/view/Widget/show_pic.dart';

import 'package:roadmap/view/splash.dart';


class Homepage extends StatefulWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  
  @override
  void initState() {
     final ProjController ctrl=Get.find();
     ctrl.fechuserdata();
   
    super.initState();
  }
  int ind=0;
  @override
  Widget build(BuildContext context) {
    final islandscape=MediaQuery.of(context).orientation==Orientation.landscape;
    //final latlong=Provider.of<AuthController>(context,listen: false);
    
     final ProjController ctrl=Get.find();
    return FutureBuilder(
      future: ctrl.fechuserdata().then((value) {
        if(ctrl.userimage==null){
         setState(() {
           fechimages();
         });
        }
      }),
      builder:(context, snapshot) => snapshot.connectionState==ConnectionState.waiting?const Splashsc(): DefaultTabController(
        //initialIndex: ind,
        length: 2,
      
        child: Scaffold(
          drawer:const  Maindrawer(),
          appBar: AppBar(
            
            
            
            bottom:  TabBar(
              
           
              
              tabs: [
              Tab(
                icon: Center(child: Icon(Icons.home,color: Theme.of(context).secondaryHeaderColor,),),
                text: 'Home',
              ),
              Tab(
                icon: Center(child: Icon(Icons.not_listed_location_sharp,color: Theme.of(context).secondaryHeaderColor,),),
                text:"Location" ,
                
              )
            ]),
            title:const Text('HomePage'),
            actions: [
              IconButton(onPressed: (){
              ctrl.logoutt();
             
            }, icon: Icon(Icons.logout,color: Theme.of(context).secondaryHeaderColor,)),
            
            IconButton(onPressed: (){
              showSearch(context: context, delegate:Mysearchdelegate() );
            }, icon:const Icon(Icons.search))
            
            ],
          ),
          body: TabBarView(
            

            
            
            children: [
             ctrl.imgs!.isEmpty?Center(child: Text('you have no images yet',style: TextStyle(color: Theme.of(context).primaryColor),),) 
            : RefreshIndicator(
              onRefresh: () {
                setState(() {
                  ctrl.fechimges();
                
                });
                throw'';
              },
              child: Center(
                         child: Column(
                children: [
                 Padding(
                   padding:  EdgeInsets.symmetric(vertical: islandscape?5:25),
                   child: CachedNetworkImage(
                        
                        fit: BoxFit.fill,
                            imageUrl:ctrl.userimage!,
                            imageBuilder: (context, imageProvider) => Container(
                              height: 120,
                              width: 120,
                              decoration:  BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(image: imageProvider,fit: BoxFit.fill)
                              ),
                            ),
                            placeholder: (context, url) => const CircularProgressIndicator(),
                            errorWidget: (context, url, error) => const Icon(Icons.error),
                         ),
                 ),
                 Expanded(child:islandscape? GridView.builder(
              // ignore: sort_child_properties_last
           itemCount: ctrl.imgs!.length,
           itemBuilder: (context, index) => Showimg(ctrl.imgs![index]),
              
              
              
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 350,
          childAspectRatio: 4 / 3,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
        ),):ListView.builder(itemBuilder:((context, index) =>  Showimg(ctrl.imgs!.reversed.toList()[index])),itemCount: ctrl.imgs!.length,))
                ],
                         ),
                       ),
            ),
           Column(
             children: [
               SizedBox(
                height: 100,
                 child: SingleChildScrollView(
                   child: Column(
                     children: [
                      const  Segmentcontroll(),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: IconButton(onPressed:()=> ctrl.updatelocation(), icon:Icon(Icons.location_on,color:Theme.of(context).primaryColor,)),
                      ),
                      GetBuilder(init: ctrl,builder:(_)=> Text(ctrl.userlocation,style: TextStyle(color:Theme.of(context).primaryColor,fontSize: 15 ),)),
                        GetBuilder(init:ctrl,builder:(_)=> Text(ctrl.address,style: const TextStyle(color: Colors.grey),)),
                     const SizedBox(height: 15,),
                     //Text(Provider.of<AuthController>(context).address,style: const TextStyle(color: Colors.grey),),
                   
                     ],
                   ),
                 ),
               ),
                Expanded(child:GridView.builder(
                 // ignore: sort_child_properties_last
                        itemCount: ctrl.imgs!.length,
                        itemBuilder: (context, index) => Showimg(ctrl.imgs![index]),
                 
                 
                 
                 gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                       maxCrossAxisExtent: 200,
                       childAspectRatio: 3 / 2,
                       crossAxisSpacing: 20,
                       mainAxisSpacing: 20,
                     ),))
             ],
           )
          ]),
          
          floatingActionButton: FloatingActionButton(backgroundColor: Theme.of(context).primaryColor,onPressed: () {
            imagepicker(ImageSource.camera).then((value) => ctrl.addigmdb(_pickedimg!));
          
          
          
          },child:  Icon( Icons.camera_enhance,color: Theme.of(context).secondaryHeaderColor,),
        ),
        ),

        
      ),
    );
    
  }
    final ImagePicker _picker=ImagePicker();
  File ?_pickedimg;
   Future<void>imagepicker(ImageSource src)async{
   final pickedimg= await _picker.pickImage(source: src);
   setState(() {
     _pickedimg=File(pickedimg!.path);
   });
  

    

  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:roadmap/view/auth_screen.dart';

import '../controller/getx_controller.dart';
class Pageviwer extends StatelessWidget {
  static const routename='/pagev';
  
  const Pageviwer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ProjController ctrl=Get.find();
    return Scaffold(
      body: Stack(
        children:[
          PageView(
            onPageChanged: (value) => ctrl.changeindex(value),
         
          children: const [
             Center(
              child: Text('this is first page',style: TextStyle(color:Colors.tealAccent),),
            ),
              Center(
              child: Text('this is secound page',style: TextStyle(color:Colors.tealAccent),),
            ),
        Center(
              child: Text('this is third page',style: TextStyle(color:Colors.tealAccent),),
            )
      
      
          ],
        ),
        Container(
          alignment: Alignment.bottomCenter,
          width: double.infinity,
          margin: const EdgeInsets.symmetric(horizontal: 6,vertical: 20),
          child: ElevatedButton(onPressed: (){
            Get.to(const Authsc());
          },style: ButtonStyle(
            backgroundColor:MaterialStateProperty.all(Theme.of(context).primaryColor),
          ), child:const Text('Go to Auth Screen',style: TextStyle(color: Colors.white),),),
        ),
        GetBuilder(init: ctrl,builder:(_)=> Indecator(ctrl.index)),
        
        ] 
      ),
    );
    
  }
}
class Indecator extends StatelessWidget {
  final int index;
   const Indecator(this.index, {super.key});

  @override
  Widget build(BuildContext context) {
   return Align(
    alignment: Alignment.bottomCenter,
     child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
        children: [
          buildcontainer(0, context),
          buildcontainer(1, context),
          buildcontainer(2, context),
        ],
      ),
   );

    
  }
  buildcontainer(int i,BuildContext context){
    return index==i? Icon(Icons.star,color: Theme.of(context).primaryColor,):Container(
      height: 15,
      width: 15,
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.grey
      ),
    );

  }
}
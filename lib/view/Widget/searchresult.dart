import 'package:flutter/material.dart';
class Result extends StatelessWidget {
  final String img;
  const Result(this.img, {super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            
            expandedHeight: 400,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              background: Image.network(img,fit: BoxFit.fill,),
            
              
            ),

          )
        ],
      ),
    );
    
  }
}
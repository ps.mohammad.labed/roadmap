import 'package:flutter/material.dart';
import 'package:get/get.dart';


import '../../controller/getx_controller.dart';
class Maindrawer extends StatelessWidget {
  const Maindrawer({Key? key}) : super(key: key);
  

  @override
  Widget build(BuildContext context) {
    final ProjController ctrl=Get.find();
 
    
    
    return Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children:[
          GetBuilder(init: ctrl,builder:(_)=> Switch(value:ctrl.isdarktheme, onChanged:(val)=> ctrl.changetheme())),
         const SizedBox(height: 8,),
       GetBuilder(init: ctrl,builder:(_)=> Text(ctrl.isdarktheme?"Switch to light theme":"Switch to dark theme",style: TextStyle(color: Theme.of(context).primaryColor,))),
        ]
      ),

    );
    
  }
}

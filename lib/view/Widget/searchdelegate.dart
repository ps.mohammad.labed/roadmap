import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:roadmap/view/Widget/searchresult.dart';

import '../../controller/getx_controller.dart';

class Mysearchdelegate extends SearchDelegate{
  
  @override
  List<Widget>? buildActions(BuildContext context) {
    IconButton(onPressed: (){
    if(query.isEmpty){
      close(context,null);
    }
    else{
      query='';
    }
   }, icon:const  Icon(Icons.cancel_outlined));
    return null;
   
  }

  @override
  Widget? buildLeading(BuildContext context) {
  return  IconButton(onPressed: (){
    close(context, null);
   }, icon:const  Icon(Icons.arrow_back));
    
   
  }

  @override
  Widget buildResults(BuildContext context) {
    return Result(query);
    
  }

  @override
  Widget buildSuggestions(BuildContext context) { 
  final ProjController ctrl=Get.find();
  
   
   return GetBuilder(
    init: ctrl,
     builder:(_)=> ListView.builder(itemBuilder:((context, index) {
      final segg= ctrl.imgs![index];
      
      return ListTile(
      onTap: () {
        query=segg;
      },
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CircleAvatar(
            radius: 25,
            backgroundImage: NetworkImage(segg),
          ),
         const SizedBox(width: 6,),
          Expanded(child: Text(segg)),
   
        ],
      )
   
     );} ),itemCount:ctrl.imgs!.length ,),
   ); 
    
  }

}
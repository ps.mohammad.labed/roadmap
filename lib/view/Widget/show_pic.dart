import 'package:flutter/material.dart';
class Showimg extends StatelessWidget {
  final String image;
  const Showimg(this.image, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      width: double.infinity,
      margin:const  EdgeInsets.symmetric(vertical: 10,horizontal: 8),
      child: Card(
        elevation: 4,
        child: ClipRRect(borderRadius: BorderRadius.circular(12),
        child: Image.network(image,fit: BoxFit.fill,),
        ),
      ),
    );
  }
}
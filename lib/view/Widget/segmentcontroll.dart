import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


import '../../controller/getx_controller.dart';
class Segmentcontroll extends StatelessWidget {
  const Segmentcontroll({Key? key}) : super(key: key);

  

  @override
  Widget build(BuildContext context) {
    final ProjController ctrl=Get.find();
   
    return GetBuilder(
      init: ctrl,
      builder:(_)=> Container(
        padding:const EdgeInsets.all(8),
        margin:const  EdgeInsets.all(5),
        child: CupertinoSlidingSegmentedControl(
          backgroundColor: Colors.white,
          thumbColor: Theme.of(context).primaryColor,
          groupValue: ctrl.segmentindex,
          onValueChanged:((value) {
          ctrl.changesegmentindex(int.parse(value.toString()));
           
          } ),
       children: {
        0:buildcontainer(context, 0, "first"),
        1:buildcontainer(context, 1, "secound"),
        2:buildcontainer(context, 2, "third"),
       }
       
      
       
        ),
      ),
    );
    
  }
  buildcontainer(BuildContext context,int i,String text){
    final ProjController ctrl=Get.find();
    return Container(
      //color: index==i?Theme.of(context).primaryColor:Theme.of(context).secondaryHeaderColor,
      padding:const  EdgeInsets.symmetric(vertical: 8,horizontal: 12),
      child: Text(text,style: TextStyle(color:ctrl.segmentindex==i?Theme.of(context).secondaryHeaderColor:Theme.of(context).primaryColor ),),

    );

  }
}
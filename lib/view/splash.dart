import 'package:flutter/material.dart';
class Splashsc extends StatelessWidget {
  const Splashsc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children:  [
             CircularProgressIndicator(color: Theme.of(context).secondaryHeaderColor,),
            Text('Loading ...',style: Theme.of(context).textTheme.bodyMedium,)
          ],
        ),
      ),
    );
    
  }
}
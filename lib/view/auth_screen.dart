import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:image_picker/image_picker.dart';


import '../controller/getx_controller.dart';
class Authsc extends StatefulWidget {
  static const routename='/authsc';
   const Authsc({Key? key}) : super(key: key);

  @override
  State<Authsc> createState() => _AuthscState();
}

class _AuthscState extends State<Authsc> with SingleTickerProviderStateMixin{
  final _formkey=GlobalKey<FormState>();

  final _passnode=FocusNode();

  final _confirmpassnode=FocusNode();

  final passctrl=TextEditingController();

  late AnimationController animctrl;

   


 Map<String,String> info={
  'email':'',
  'password':'',
 };
@override
  void initState() {
    animctrl=AnimationController(vsync: this,duration:const  Duration(seconds: 3));
   
    super.initState();
  }
  final ImagePicker _picker=ImagePicker();
  File ?_pickedimg;
  imagepicker(ImageSource src)async{
   final pickedimg= await _picker.pickImage(source: src);
   setState(() {
     _pickedimg=File(pickedimg!.path);
   });

    

  }
    onsubmit(){
  final ProjController ctrl=Get.find();
  if(_formkey.currentState!.validate()){
    _formkey.currentState!.save();
    ctrl.issignup?ctrl.signup(info['email']!, info['password']!,_pickedimg!,context):ctrl.signin(info['email']!, info['password']!,context);

  } else{
    return;
  }
  
}

  @override
  Widget build(BuildContext context) {
   final ProjController ctrl=Get.find();
    return Scaffold(
    
      body: SingleChildScrollView(
        child: Column(
          children: [
           Stack(
             children:[
              ClipPath(
               clipper: CustomClipPath(),
               child: Container(
                 width: MediaQuery.of(context).size.width,
                 height: 200,
                 color: Colors.teal,
               ),
             ),
             Align(
              alignment: Alignment.center,
               child: Container(
                margin: const EdgeInsets.only(top: 25),
                height: 150,
                width: 150,
                decoration: BoxDecoration(shape: BoxShape.circle,border: Border.all(width: 3,color: Theme.of(context).secondaryHeaderColor)),
                child:  Center(child: Icon(Icons.person,size: 50,color: Theme.of(context).secondaryHeaderColor,)),
               ),
             )
             ] 
           ),
          
            Padding(
              padding: const EdgeInsets.all(22.0),
              child: Card(
                elevation: 4,
                child: Container(
                  padding:const  EdgeInsets.all(15),
                  child: GetBuilder(
                    init: ctrl,
                    builder:(_)=> Form(
                      key: _formkey,
                      child:  SingleChildScrollView(
                        child: Column(
                          children: [
                      ctrl.issignup?    Column(
                            children: [
                               CircleAvatar(
                                  radius: 40,
                                  backgroundColor: Colors.grey,
                                  backgroundImage:_pickedimg==null?null:FileImage(_pickedimg!),
                                  
                                ),
                                 Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                             children: [
                            TextButton.icon(onPressed: ()=>imagepicker(ImageSource.gallery), icon:const  Icon(Icons.image),label:Container(constraints: const BoxConstraints(maxWidth: 80),child: const  Text('Pick Image From Galary')),),
                              TextButton.icon(onPressed: ()=>imagepicker(ImageSource.camera), icon:const  Icon(Icons.camera),label:Container(constraints: const BoxConstraints(maxWidth: 80),child: const  Text('Pick Image From Camera')),),
                                             
                                                     ],
                                                   ),
                            ],
                          ):const SizedBox(),
                           
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                icon: Icon(Icons.person,color: Theme.of(context).iconTheme.color,),
                                label:const Text('E-mail'),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(12),),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12),borderSide: BorderSide(color: Theme.of(context).primaryColor)),
                      
                      
                              ),
                      
                              onFieldSubmitted: (value) {
                                FocusScope.of(context).requestFocus(_passnode);
                              },
                              validator: (value) {
                                if(value!.isEmpty||!value.contains('@')){
                                  return 'invalid email';
                                }
                                return null;
                              },
                              onSaved: (newValue) {
                              setState(() {
                                info['email']=newValue!;
                                
                              });
                              },
                              
                            ),
                            const SizedBox(height: 8,),
                             TextFormField(
                              controller: passctrl,
                              focusNode: _passnode,
                              obscureText: ctrl.securepass,
                      
                      
                              keyboardType: TextInputType.visiblePassword,
                              decoration: InputDecoration(
                                 icon:InkWell(onTap: ()=>ctrl.changepasssecure(),child:  Icon(Icons.remove_red_eye,color: Theme.of(context).iconTheme.color,)),
                                  
                                
                                label:const Text('Password'),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(12),),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12),borderSide: BorderSide(color: Theme.of(context).primaryColor)),
                      
                      
                              ),
                      
                              onFieldSubmitted: (value) {
                                FocusScope.of(context).requestFocus(_confirmpassnode);
                              },
                              validator: (value) {
                                if(value!.isEmpty||value.length<6){
                                  return 'Weak Password';
                                }
                                return null;
                              },
                              onSaved: (newValue) {
                                setState(() {
                                  info['password']=newValue!;
                                });
                              },
                              
                            ),
                            const SizedBox(height: 8,),
                            
                            AnimatedContainer(
                              constraints: BoxConstraints(minHeight: ctrl.issignup?60:0,maxHeight: ctrl.issignup?120:0),
                              duration:const Duration(seconds: 3),
                              child: !ctrl.issignup?const SizedBox() :TextFormField(
                                
                                obscureText: ctrl.securepass,
                                
                                focusNode: _confirmpassnode,
                                keyboardType: TextInputType.visiblePassword,
                                decoration: InputDecoration(
                                  icon: Icon(Icons.check_box_outlined,color: Theme.of(context).iconTheme.color,),
                                  label:const Text('Confirm Password'),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(12),),
                                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12),borderSide: BorderSide(color: Theme.of(context).primaryColor)),
                                        
                                        
                                ),
                                        
                                
                                validator: (value) {
                                  if(value!=passctrl.text){
                                    return 'Passwords dont match';
                                  }
                                  return null;
                                },
                               
                                
                              ),
                            ),
                          const SizedBox(height: 12,),
                          ElevatedButton(onPressed: onsubmit,
                            child: Text(ctrl.issignup?"Sign-Up":"Signin")),
                          TextButton(onPressed:()=> ctrl.changesignmode(), child: Text(ctrl.issignup?"Login instead":"Create Account")),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  
    
  }
}

class CustomClipPath extends CustomClipper<Path> {
  
  @override
  Path getClip(Size size) {
    var path =  Path();
    path.lineTo(0.0, 40.0);
    path.lineTo(0.0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 40.0);

    for (int i = 0; i < 10; i++) {
      if (i % 2 == 0) {
        path.quadraticBezierTo(
            size.width - (size.width / 16) - (i * size.width / 8),
            0.0,
            size.width - ((i + 1) * size.width / 8),
            size.height - 160);
      } else {
        path.quadraticBezierTo(
            size.width - (size.width / 16) - (i * size.width / 8),
            size.height - 120,
            size.width - ((i + 1) * size.width / 8),
            size.height - 160);
      }
    }

    path.lineTo(0.0, 40.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
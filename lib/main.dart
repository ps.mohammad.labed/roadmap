import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:roadmap/controller/getx_controller.dart';


import 'package:roadmap/view/auth_screen.dart';
import 'package:roadmap/view/pageview.dart';

void main()async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

 
  @override
  Widget build(BuildContext context) {
  Get.put(ProjController());
  ProjController ctrl=Get.find();
  
    return 
     GetBuilder<ProjController>(
      init:ctrl ,
       builder:(_)=> MaterialApp(
           
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
        
      //checks with getx if user choosen dark or light theme mode,
        themeMode:ctrl.isdarktheme?ThemeMode.dark:ThemeMode.light ,
          darkTheme:  ThemeData(
            canvasColor: const Color.fromARGB(255, 2, 18, 32),
            textTheme:const  TextTheme(
              bodyMedium: TextStyle(color: Colors.white,fontFamily: 'Lato')
            ),
            
            primarySwatch: Colors.teal,
            secondaryHeaderColor: Colors.white,
            iconTheme:const  IconThemeData(color: Colors.teal)
          ),
          theme: 
          ThemeData(
            textTheme:const  TextTheme(
              bodyMedium: TextStyle(color: Colors.white,fontFamily: 'Lato')
            ),
            
            primarySwatch: Colors.teal,
            secondaryHeaderColor: Colors.white,
            iconTheme:const  IconThemeData(color: Colors.teal)
          ),
          //home: Homepage(),
        routes: {
          '/':(_) =>const Pageviwer(),
          Authsc.routename:(_) =>const Authsc() 
        },
        ),
     );
    
  }
}


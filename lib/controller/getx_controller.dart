// ignore_for_file: avoid_print

import 'dart:io';
//this is the Getx controller file  here you find all functions and variables of the project

import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/db_helper.dart';
import '../view/auth_screen.dart';
import '../view/home_page.dart';
class ProjController extends GetxController{
   double lat=0.0;
  double long=0.0;
  int index=0;
  int segmentindex=0;
  bool isdarktheme=false;
String?token;
  List ?imgs=[];
  bool securepass=true;
  String userlocation="";
  bool issignup=false;
  String ?userimage;
  String address='';
  bool get authed{
    return isAuth;
  }
  Future<void>updatelocation()async{
    //this called every time a  user presses on get location button
    Position pos=await _determinePosition();
    List pm=await placemarkFromCoordinates(lat, long);
    lat=pos.latitude;
    long=pos.longitude;
    address=pm[0].toString();
    userlocation='$lat ,$long';
    update();


    
  }
  Future<Position> _determinePosition() async {
    //function of the location using Geo Locator
  bool serviceEnabled;
  LocationPermission permission;

  // Test if location services are enabled.
  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    // Location services are not enabled don't continue
    // accessing the position and request users of the 
    // App to enable the location services.
    return Future.error('Location services are disabled.');
  }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // Permissions are denied, next time you could try
      // requesting permissions again (this is also where
      // Android's shouldShowRequestPermissionRationale 
      // returned true. According to Android guidelines
      // your App should show an explanatory UI now.
      return Future.error('Location permissions are denied');
    }
  }
  
  if (permission == LocationPermission.deniedForever) {
    // Permissions are denied forever, handle appropriately. 
    return Future.error(
      'Location permissions are permanently denied, we cannot request permissions.');
  } 

  // When we reach here, permissions are granted and we can
  // continue accessing the position of the device.
  Position p= await Geolocator.getCurrentPosition();
  lat=p.latitude;
  long=p.longitude;
  update();
  return p;
}
  changesegmentindex(int val){
    //update index of the segment controll when the user change the slide of the segment controll
    segmentindex=val;
    update();
  }
  changeindex(int val){
    //change index for the tab bar 
    index=val;
    update();
  }
  changetheme()async{
    // this called in the drawer when the user change the theme mode using the swich button
    isdarktheme=!isdarktheme;
    update();
    final pref=await SharedPreferences.getInstance();
    pref.setBool('thememode', isdarktheme);
  }
  changepasssecure(){
    // in the sign form  called when user want to make his password visible
    securepass=!securepass;
    update();
  }
  changesignmode(){
    // in sign form wheather user wants sign up or sign in mode
    issignup=!issignup;
    update();
  }
  signup(String email,String password,File img,BuildContext context)async{
    // this call the authentication method and gives it the user informations then inserts it to the database
    
 await authintecate('signUp', email, password).then((value) {
 Navigator.pushReplacement(context, MaterialPageRoute(builder: ((context) =>authed? const Homepage():const Authsc())));
 });
 await insertusertodb(email, password, img);
   
  }
  signin(String email,String password,BuildContext context)async{
     final pref=await SharedPreferences.getInstance();
     isdarktheme= pref.containsKey('thememode')?pref.getBool('thememode')!:false;
     userlocation= pref.getString('location')??'';
     lat=pref.getDouble('lat')??0;
       long=pref.getDouble('long')??0;
     
     update();
   

    try{
      
await authintecate('signInWithPassword', email, password).then((value) {
 authed?Navigator.pushReplacement(context, MaterialPageRoute(builder: ((context) =>const Homepage()))):showDialog(context: context, builder: ((context) => const SizedBox(
  height: 100,
   child: AlertDialog(
    scrollable: true,
    
    content:Center(child: Text('Error occured try aggain ',style: TextStyle(color: Colors.red),),),
title: Text('Error!!'),
   ),
 )));
});
    }
    catch(e){
      print('$e iiiiiiiiiiiiiiiiiiiiiiiiiiii');
      
    }
      
     
   
    
  }
  Future<void>fechuserdata()async{
    //fech users data likepersonal data and images that they added to the data base
    try{
  await fetchfromdbuserinfo();
  await fechimges();
  
    userimage=userimg;
    print('$userimage');
    update();
    }
    catch(e){
      print('$e ppppppppppppppppppppppppppppppppp');
    }
  
  }
  logoutt()async{
  
     await logout();
     
address='';
    userimage='';
    userlocation='';
    imgs!.clear();
     DefaultCacheManager manager =  DefaultCacheManager();
 manager.emptyCache();
 update();
    

  }
  
addigmdb(File img)async{
  // this called when user take picture and add it to the database
  await addimage(img);
  
  update();

}
fechimges()async{
  //get images that been added to the db by the user
  await fechimages();
  imgs=images;
  update();
}
}